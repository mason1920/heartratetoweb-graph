const timeSeries = new TimeSeries();

const smoothieChart = new SmoothieChart();
smoothieChart.addTimeSeries(timeSeries);
smoothieChart.streamTo(document.getElementById("smoothie-chart"), 500);

setInterval(() => {
    const xmlHttpRequest = new XMLHttpRequest();
    xmlHttpRequest.open("GET", "http://localhost:6547/hr", false);
    xmlHttpRequest.send();

    const current = document.getElementById("current");

    if (xmlHttpRequest.status === 200) {
        const heartrate = xmlHttpRequest.responseText;

        current.innerHTML = heartrate + " BPM";

        timeSeries.append(new Date().getTime(), heartrate);
    } else {
        current.innerHTML = "HTTP Status " + xmlHttpRequest.status;
    }
}, 500);
