# HeartRateToWeb Graph

Turns the data from a [HeartRateToWeb](https://github.com/loic2665/HeartRateToWeb) HTTP server into an HTML page with the current BPM in a header and the historic BPM in a [Smoothie Charts](https://github.com/joewalnes/smoothie) line chart.
